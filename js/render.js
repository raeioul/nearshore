
var cars =[
{id:"Toyota Corolla", price: 8000, color: "white",img:"images/corolla.jpg", key:1 },
{id:"Nissan Pathfinder", price: 12000, color: "blue", img :"images/pathfinder.jpg", key: 2},
{id:"Kia Río", price: 8000, color: "red", img:"images/kiario.jpg", key:3},
{id:"Suzuki Ignis", price: 12400, color: "orange" ,img:"images/ignis.jpg", key:4}
];

var app = {

ini: function(){
$("#app-render").html(this.render(cars));
},

render: function(cars) {
var htmlCars= [];
 for(var i in cars){

    htmlCars.push(this.template(cars[i].id, cars[i].img, cars[i].key,  cars[i].color,  cars[i].price));
 }

return htmlCars.join("\n");
},

template: function(id , image, key, color, price){
var html = [
            '<div class="w3-col l3 m6 relPos w3-center">',
        '<div id="div'+key+'" class="selectProduct w3-padding"   data-id="'+id+'" data-price="'+price+'" data-color="'+color+'" data-title="'+id.replace(/\s/g,'')+'">',
  '<a class="w3-btn-floating w3-light-grey addButtonCircular addToCompare">+</a>',

'<img src="'+image+'" class="imgFill productImg img-responsive">',

                    '<h4>'+id+'</h4>',
                '</div></div>',


];


return html.join("\n");
}

};

app.ini();
